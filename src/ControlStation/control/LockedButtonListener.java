package ControlStation.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ControlStation.view.ControlGUI;

public class LockedButtonListener implements ActionListener {
	private int lockedID;
	private ControlGUI gui;
	private ControlStationController stationController;
	
	public LockedButtonListener(int lockedID, ControlGUI gui, ControlStationController stationController){
		this.lockedID = lockedID;
		this.gui = gui;
		this.stationController = stationController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		boolean success = stationController.unlockStation(lockedID);
				
		if(success){
			gui.unlockStation(lockedID);
		}

	}

}
