package ControlStation.control;

import ControlStation.model.ControlStation;
import ControlStation.view.ControlGUI;

public class ControlStationController {
	private ControlStation station;
	private ControlGUI controlGUI;
	
	public ControlStationController(ControlStation station){
		this.station = station;
	}

	public int setOperatorInfo(String name, int ID) {
		int response = station.setOperatorInfo(name, ID);
		return response;
		
	}

	public void updateGUI(int lockedID) {
		if(controlGUI == null){
			System.out.println("How did you get here?\nThere is no control GUI yet.");
		} else {
			controlGUI.updateLockedStation(lockedID);
		}
		
	}

	public void setGUI(ControlGUI controlGUI) {
		this.controlGUI = controlGUI;
		
	}

	public boolean unlockStation(int lockedID) {
		boolean success = station.unlockStation(lockedID);
		return success;
	}

	public void initiateVotingStations() {
		station.initiateVotingStations();
		
	}

}
