package ControlStation.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.JSpinner;

import newDefault.DBManager;
import ControlStation.model.VotingStation;

public class BtnVoteListener implements ActionListener{
	private JSpinner spinner, spinner_1, spinner_2, spinner_3, spinner_4;
	private VotingStation votingStation;
	private DBManager db;
	
	public BtnVoteListener(VotingStation votingStation, JSpinner spinner,JSpinner spinner_1,JSpinner spinner_2,JSpinner spinner_3,JSpinner spinner_4, DBManager db){
		this.spinner = spinner;
		this.spinner_1 = spinner_1;
		this.spinner_2 = spinner_2;
		this.spinner_3 = spinner_3;
		this.spinner_4 = spinner_4;
		this.votingStation = votingStation;
		this.db = db;
				
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {Integer currentValue =(Integer) spinner_1.getValue();
	int flag=0;
	
	if(!(currentValue==0)){
		if(spinner_1.getValue()==spinner.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  2 and 1 are same");
		}
	}
	
	currentValue =(Integer) spinner_2.getValue();
	if(!(currentValue==0)){
		if(spinner_2.getValue()==spinner.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  3 and 1 are same");
		}
		else if(spinner_2.getValue()==spinner_1.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  3 and 2 are same");
		}
	}
	
	currentValue =(Integer) spinner_3.getValue();
	if(!(currentValue==0)){
		if(spinner_3.getValue()==spinner.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  4 and 1 are same");
		}
		else if(spinner_3.getValue()==spinner_1.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  4 and 2 are same");
		}
		else if(spinner_3.getValue()==spinner_2.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  4 and 3 are same");
		}
	}
	
	currentValue =(Integer) spinner_4.getValue();
	if(!(currentValue==0)){
		if(spinner_4.getValue()==spinner.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  5 and 1 are same");
		}
		else if(spinner_4.getValue()==spinner_1.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  5 and 2 are same");
		}
		else if(spinner_4.getValue()==spinner_2.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  5 and 3 are same");
		}
		else if(spinner_4.getValue()==spinner_3.getValue()){
			flag=1;
			JOptionPane.showMessageDialog(null,"Error vote number  5 and 4 are same");
		}
	}
	if(flag==0){
				
		try {
					db.addVote((Integer)spinner.getValue(), (Integer)spinner_1.getValue(), (Integer)spinner_1.getValue(), (Integer)spinner_1.getValue(), (Integer)spinner_2.getValue(), (Integer)spinner_3.getValue(), (Integer)spinner_4.getValue());
					votingStation.lock();
				} 
				catch (SQLException e) {
					e.printStackTrace();
				}
			
			}
	}
	}