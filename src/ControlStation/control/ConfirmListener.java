package ControlStation.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ControlStation.view.OperatorRegistrarionGUI;

public class ConfirmListener implements ActionListener {
	private OperatorRegistrarionGUI GUI; 
	
	public ConfirmListener(OperatorRegistrarionGUI GUI ) {
		 this.GUI = GUI;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
			GUI.setOperatorInfo();
	}

}
