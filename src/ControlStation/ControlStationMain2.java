package ControlStation;

import ControlStation.control.ControlStationController;
import ControlStation.model.ControlStation;
import ControlStation.model.VotingStation;
import ControlStation.view.OperatorRegistrarionGUI;

public class ControlStationMain2 {
public static void main(String[] args) {
		ControlStation station = new ControlStation(1000, "224.0.0.1");
		ControlStationController stationController = new ControlStationController(station);
		station.setController(stationController);
		new OperatorRegistrarionGUI(stationController);
		station.start();
		
		VotingStation dummy1 = new VotingStation(1001, "224.0.0.1");
	}
}
