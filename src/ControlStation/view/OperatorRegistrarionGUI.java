package ControlStation.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import ControlStation.control.ConfirmListener;
import ControlStation.control.ControlStationController;

public class OperatorRegistrarionGUI extends JFrame {
	//middle man between actual controller model and GUI
	private ControlStationController stationController;
	
	private JPanel initialPanel = new JPanel(new BorderLayout(50, 50));
	
	//TODO change fonts and size
	private JLabel basicInfoLabel = new JLabel("Welcome. Please insert your information below. This information will be stored for security purposes. "); //shows info on the person controlling the station
	private JLabel askForName = new JLabel("Insert your full name:"); 
	private JLabel askForID = new JLabel("Insert your controller ID:");
	
	private JTextField nameInput = new JTextField();
	private JTextField idInput = new JTextField();
	
	private JButton confirmInfo = new JButton("Confirm");
		
	public OperatorRegistrarionGUI(ControlStationController stationController){
		this.stationController = stationController;
		
		initializeFrame();
		populateMainPanel();
		finishFrameCreation();
	}

	private void populateMainPanel() {
		JPanel coverLayout = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel centralPanel = new JPanel(new GridLayout(3,2, 10, 10));
		
		confirmInfo.addActionListener(new ConfirmListener(this));
		
		centralPanel.add(askForName);
		centralPanel.add(nameInput);
		centralPanel.add(askForID);
		centralPanel.add(idInput);
		centralPanel.add(new JPanel());
		centralPanel.add(confirmInfo);
		
		coverLayout.add(centralPanel);
		initialPanel.add(coverLayout, BorderLayout.CENTER);
		
		JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		topPanel.add(basicInfoLabel);
		initialPanel.add(topPanel, BorderLayout.NORTH);
	}

	private void finishFrameCreation() {
		setContentPane(initialPanel);
		setVisible(true);
	}

	private void initializeFrame() {
		setTitle("**********Control Station**********");
		setLocation(10, 10);
		setExtendedState(JFrame.MAXIMIZED_BOTH);  
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setUndecorated(true);
	}

	public void setOperatorInfo() {
		try{
			int operatorID = Integer.parseInt(idInput.getText());
			String operatorName = nameInput.getText();
			int response = stationController.setOperatorInfo(operatorName, operatorID);
			
			if(response == 1){
				showControlGUI();
			} else {
				showOperatorError();
			}
			
		} catch(NumberFormatException e){
			showIDErrorMessage();
		}
	}

	private void showOperatorError() {
		JOptionPane operatorError = new JOptionPane();
		operatorError.showMessageDialog(this, "An operator is already registered for this station.\nPlease inform this error to a superior ");
	}

	private void showControlGUI() {
		showGreetings();
		
		ControlGUI controlGUI = new ControlGUI(stationController);
		stationController.setGUI(controlGUI);
		
		confirmInfo.setEnabled(false);
		nameInput.setEnabled(false);
		idInput.setEnabled(false);
		
		this.setVisible(false);
	}

	private void showGreetings() {
		JOptionPane IDError = new JOptionPane();
		IDError.showMessageDialog(this, "Hello, "+nameInput.getText()+". You are now registered as this station's controller. ");
		
	}

	private void showIDErrorMessage() {
		JOptionPane IDError = new JOptionPane();
		IDError.showMessageDialog(this, "The ID must be numerical only");
		this.idInput.setText("");
	}
}
