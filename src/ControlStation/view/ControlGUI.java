package ControlStation.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import ControlStation.control.ControlStationController;
import ControlStation.control.LockedButtonListener;

public class ControlGUI extends JFrame {
	private int qttVotingStations = 6;

	private ControlStationController stationController;

	private JButton[] controlButtons = new JButton[qttVotingStations];

	private JPanel mainPanel = new JPanel(new BorderLayout(20, 20));

	// TODO fill with general info
	private JLabel generalInfoLabel = new JLabel("Presidential election 2014");

	public ControlGUI(ControlStationController stationController) {
		this.stationController = stationController;

		initializeFrame();
		populateMainPanel();
		finishFrameCreation();
		stationController.initiateVotingStations();
	}

	private void populateMainPanel() {
		JPanel buttonsPanel = new JPanel(new GridLayout(2, 3, 60, 60));
		JPanel stationPanel;
		JLabel stationInfoLabel;

		// TODO add controllers to the buttons
		for (int i = 0; i < qttVotingStations; i++) {
			controlButtons[i] = new JButton("Waiting...");
			controlButtons[i].setEnabled(false);
			stationInfoLabel = new JLabel("Station " + (i + 1));

			stationPanel = new JPanel(new GridLayout(2, 1));
			stationPanel.add(stationInfoLabel);
			stationPanel.add(controlButtons[i]);

			buttonsPanel.add(stationPanel);
		}

		mainPanel.add(buttonsPanel, BorderLayout.CENTER);
		mainPanel.add(generalInfoLabel, BorderLayout.NORTH);
		mainPanel.add(new JPanel(new GridLayout(2, 2, 10, 10)),
				BorderLayout.EAST);
		mainPanel.add(new JPanel(new GridLayout(2, 2, 10, 10)),
				BorderLayout.WEST);
		mainPanel.add(new JPanel(new GridLayout(2, 2, 10, 10)),
				BorderLayout.SOUTH);
	}

	private void initializeFrame() {
		setTitle("**********Control Station**********");
		setLocation(10, 10);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		//setUndecorated(true);
	}

	private void finishFrameCreation() {
		setContentPane(mainPanel);
		setVisible(true);
	}

	public void updateLockedStation(int lockedID) {
		if (-1 < lockedID && lockedID < controlButtons.length) {
			controlButtons[lockedID].setText("Locked");
			controlButtons[lockedID].setEnabled(true);
			if (controlButtons[lockedID].getActionListeners() != null)
				controlButtons[lockedID]
						.addActionListener(new LockedButtonListener(lockedID,
								this, stationController));

		}
	}

	public void unlockStation(int lockedID) {
		controlButtons[lockedID].setText("Waiting...");
		controlButtons[lockedID].setEnabled(false);
	}
}
