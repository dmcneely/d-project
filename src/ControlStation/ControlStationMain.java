package ControlStation;

import java.util.Timer;

import ControlStation.control.ControlStationController;
import ControlStation.model.ControlStation;
import ControlStation.model.VotingStation;
import ControlStation.view.OperatorRegistrarionGUI;

public class ControlStationMain {

	public static void main(String[] args) {
		
		VotingStation dummy1 = new VotingStation(1001, "224.0.0.1");
		VotingStation dummy2 = new VotingStation(1002, "224.0.0.1");
		VotingStation dummy3 = new VotingStation(1003, "224.0.0.1");
		VotingStation dummy4 = new VotingStation(1004, "224.0.0.1");
		VotingStation dummy5 = new VotingStation(1005, "224.0.0.1");
		VotingStation dummy6 = new VotingStation(1006, "224.0.0.1");
		
		ControlStation station = new ControlStation(1000, "224.0.0.1");
		ControlStationController stationController = new ControlStationController(station);
		station.setController(stationController);
		new OperatorRegistrarionGUI(stationController);
		station.start();
	}

}
