package ControlStation.model;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;

import newDefault.DBManager;
import ControlStation.control.BtnVoteListener;

public class VotingStation extends JFrame {
	private boolean isLocked = false;
	private boolean isRunning = false;
	
	// station ID is composed of the control-station ID followed by numbers 1-x,
	// where x is the amount of stations controlled.
	private int stationID;
	private VotingStationReceiver rec;

	// message to be sent when a station locks
	private String lockMsg;
	// info for connecting to the multicast server: the control station
	int port = 4003;
	private String group;
	private MulticastSocket s;
	private final DBManager db = new DBManager();		//Connecting to the database
	
	//UI frame
	private JPanel votingPane, lockedPane;

	public VotingStation(int stationID, String controlStationGroup) {
		this.stationID = stationID;
		this.group = controlStationGroup;
		lockMsg = "Station locked: " + stationID;
		
		try {
			initializeFrame();
			connectToControlGroup();
			receiveMessage();
			start();
			this.setVisible(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initializeFrame(){
		
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		//setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 456, 443);
	}
	
	private void start() {
		
		votingPane = new JPanel();
		votingPane.setForeground(new Color(0, 0, 0));
		votingPane.setBackground(Color.WHITE);
		setContentPane(votingPane);
		votingPane.setLayout(new GridLayout(7, 4, 0, 0));


		JLabel lblParty = new JLabel("PARTY");
		lblParty.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(lblParty);
		
		JLabel lblCandidates = new JLabel("CANDIDATES");
		lblCandidates.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(lblCandidates);
		
		JLabel label = new JLabel("");
		label.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(label);
		
		JLabel lblVotes = new JLabel("VOTES");
		lblVotes.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(lblVotes);
		
		JLabel plb1 = new JLabel("");
		plb1.setIcon(new ImageIcon("Pics/Party2.png"));
		plb1.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(plb1);
		
		JLabel clb1 = new JLabel("<html> KEARNS <br>(ROBERT KEARNS of 4 Castle Park, Wicklow, Co. Wicklow; carpenter </html>");
		clb1.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(clb1);
		
		JLabel piclb1 = new JLabel("");
		piclb1.setIcon(new ImageIcon("Pics/Candidate1.png"));
		piclb1.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(piclb1);
		
		final JSpinner spinner = new JSpinner();
		spinner.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		votingPane.add(spinner);
		
		JLabel plb2 = new JLabel("");
		plb2.setIcon(new ImageIcon("Pics/Party2.png"));
		plb2.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(plb2);
		
		JLabel clb2 = new JLabel("<HTML> KEDDY <br> CHARLIE KEDDY of Sea Road, Kilcoole, Co. Wicklow; Plumber </HTML>");
		clb2.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(clb2);
		
		JLabel piclb2 = new JLabel("");
		piclb2.setIcon(new ImageIcon("Pics/Candidate2.png"));
		piclb2.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(piclb2);
		
		final JSpinner spinner_1 = new JSpinner();
		spinner_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner_1.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		votingPane.add(spinner_1);
		
		JLabel plb3 = new JLabel("");
		plb3.setIcon(new ImageIcon("Pics/Party5.png"));
		plb3.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(plb3);
		
		JLabel clb3 = new JLabel("<HTML> KELLY - THE LABOUR PARTY <br> NICKY KELLY of Aille Highfield Avenue, Arklow, Co Wicklow; Full Time Public Representative </HTML>");
		clb3.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(clb3);
		
		JLabel piclb3 = new JLabel("");
		piclb3.setIcon(new ImageIcon("Pics/Candidate3.png"));
		piclb3.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(piclb3);
		
		final JSpinner spinner_2 = new JSpinner();
		spinner_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner_2.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		votingPane.add(spinner_2);
		
		JLabel plb4 = new JLabel("");
		plb4.setIcon(new ImageIcon("Pics/party1.png"));
		plb4.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(plb4);
		
		JLabel clb4 = new JLabel("<HTML>KENNEDY - SOCIALIST WORKERS PARTY (S.W.P.)<br> CATHERINE KENNEDY of 31 Old Court Drive, Bray, Co. Wicklow </HTML>");
		clb4.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(clb4);
		
		JLabel piclb4 = new JLabel("");
		piclb4.setIcon(new ImageIcon("Pics/Candidate4.png"));
		piclb4.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(piclb4);
		
		final JSpinner spinner_3 = new JSpinner();
		spinner_3.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner_3.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		votingPane.add(spinner_3);
		
		JLabel plb5 = new JLabel("");
		plb5.setIcon(new ImageIcon("Pics/Party5.png"));
		plb5.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(plb5);
		
		JLabel clb5 = new JLabel("<HTML>McManus - the labour party <br> LIZ McMANUS of 1 Martello Terrace, bray, Co. Wicklow; Full Time Public Representative  </HTML>");
		clb5.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(clb5);
		
		JLabel piclb5 = new JLabel("");
		piclb5.setIcon(new ImageIcon("Pics/Candidate5.png"));
		piclb5.setBorder(new LineBorder(new Color(0, 0, 0)));
		votingPane.add(piclb5);
		
		final JSpinner spinner_4 = new JSpinner();
		spinner_4.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner_4.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		votingPane.add(spinner_4);
		
		JButton btnvote = new JButton("Vote");
		btnvote.addActionListener(new BtnVoteListener(this, spinner, spinner_1, spinner_2, spinner_3, spinner_4, db));
	
		votingPane.add(btnvote);
		this.revalidate();
		this.repaint();
	}
		

	// connects to the control station via a multicast socket, which listens
	// constantly for messages from the group
	// the messages will be filtered by stationID
	private void connectToControlGroup() throws IOException,
			UnknownHostException {
		s = new MulticastSocket(port);
		s.joinGroup(InetAddress.getByName(group));
	}

	private void receiveMessage() throws IOException {
		rec = new VotingStationReceiver(s, this);
	}

	public void processMessage(String msg) {
		if (msg.contains("Unlock") && isLocked == true) {
			String[] msgParts = msg.split("\\s+");
			try {
				int stationID = Integer.parseInt(msgParts[msgParts.length-1]);
				if(stationID == this.stationID){
					isLocked = false;
					start();
				}
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
			}
		}

	}

	//locks the screen
	public void lock() {
		this.isLocked = true;
		
		//changes layout
		JLabel lockedMsg = new JLabel("*******************This station is locked*******************");
		lockedPane = new JPanel(new BorderLayout());
		lockedPane.add(lockedMsg, BorderLayout.CENTER);
		this.setContentPane(lockedPane);
		this.revalidate();
		this.repaint();
		
		//informs control station
		byte buf[] = lockMsg.getBytes();
		// Create and send the DatagramPacket
		DatagramPacket pack;
		try {
			pack = new DatagramPacket(buf, buf.length,
					InetAddress.getByName(group), port);
			int ttl = s.getTimeToLive();
			s.setTimeToLive(ttl);
			s.send(pack);
			System.out.println("Voting Station "+stationID+": message sent");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}