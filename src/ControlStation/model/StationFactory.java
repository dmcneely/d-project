package ControlStation.model;


public class StationFactory {
	private int qttControlStations = 0;
	private int qttVotingStPerControlSt = 6;
	private String groupIP = "244.0.0.";
	
	public ControlStation createControlStation(){
		qttControlStations++;
		int controlStationID = 1000+(qttControlStations*10);
		String newGroup = groupIP+qttControlStations; 
		ControlStation cs = new ControlStation(controlStationID, newGroup);
		return cs;
	}
	
	public VotingStation createVotingStationFor(ControlStation cs){
		int qttVotingStations = cs.getQttVotingStations();
		int controlStationID = cs.getStationID();
		int group = (controlStationID/10) - 1000;
		
		VotingStation vs = new VotingStation(controlStationID + qttVotingStations, groupIP+group);
		
		cs.incrementVotingStations();
		
		return vs;
		
	}
	

}
