package ControlStation.model;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

public class ControlStationReceiver implements Runnable {
	private MulticastSocket s;
	private ControlStation controlStation;
	private Thread listener = new Thread(this);
	
	public ControlStationReceiver(MulticastSocket s, ControlStation controlStation){
		this.s = s;
		this.controlStation = controlStation;
		
		listener.start();
	}
	
	@Override
	public void run() {
		byte[] buf;
		while(true) 
			try { 
				buf = new byte[1024]; 
				DatagramPacket pack = new DatagramPacket(buf, buf.length);
				s.receive(pack);

				String msg = new String(pack.getData());
				msg = msg.trim();

				System.out.println(msg);
				System.out.println("Control Station: message received");
				
				controlStation.processMessage(msg);
			
			} catch(IOException e) { 
					break; 
			} 
	}

}
