package ControlStation.model;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class CopyOfDummyVotingStation {
	private boolean isLocked = false;
	private boolean isRunning = false;
	// station ID is composed of the control-station ID followed by numbers 1-x,
	// where x is the amount of stations controlled.
	private int stationID;
	private VotingStationReceiver rec;

	// message to be sent when a station locks
	private String lockMsg;
	// info for connecting to the multicast server: the control station
	int port = 5000;
	private String group;
	private MulticastSocket s;

	public CopyOfDummyVotingStation(int stationID, String controlStationGroup) {
		this.stationID = stationID;
		this.group = controlStationGroup;
		lockMsg = "Station locked: " + stationID;

		try {
			connectToControlGroup();
			receiveMessage();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// connects to the control station via a multicast socket, which listens
	// constantly for messages from the group
	// the messages will be filtered by stationID
	private void connectToControlGroup() throws IOException,
			UnknownHostException {
		s = new MulticastSocket(port);
		s.joinGroup(InetAddress.getByName(group));
	}

	private void receiveMessage() throws IOException {
//		rec = new VotingStationReceiver(s, this);
	}

	public void run() {
		if (isLocked == false) {
			this.isLocked = true;
			// send packet saying it's locked
			byte buf[] = lockMsg.getBytes();
			// Create and send the DatagramPacket
			DatagramPacket pack;
			try {
				pack = new DatagramPacket(buf, buf.length,
						InetAddress.getByName(group), port);
				int ttl = s.getTimeToLive();
				s.setTimeToLive(ttl);
				s.send(pack);
				System.out.println("Voting Station: message sent");
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void processMessage(String msg) {
		if(msg.contains("Initiate")){
			run();
		}
		
		if (msg.contains("Unlock")) {
			String[] msgParts = msg.split("\\s+");
			try {
				int stationID = Integer.parseInt(msgParts[msgParts.length-1]);
				if(stationID == this.stationID){
					this.isLocked = false;
					System.out.println("Voting Station: Unlocked.");
				}
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
			}
		}

	}
}
