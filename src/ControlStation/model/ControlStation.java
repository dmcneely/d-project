package ControlStation.model;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

import ControlStation.control.ControlStationController;

public class ControlStation {
	private ControlStationController stationController;
	private int stationID = -1;
	private int qttVotingStations = 0;
	private String operatorName = "";
	private int operatorID = -1;
	private String unlockMsg = "Unlock: ";

	int port = 4003;
	private String group;
	private MulticastSocket s;
	private ControlStationReceiver rec;

	public ControlStation(int stationID, String group) {
		this.stationID = stationID;
		this.group = group;
	}

	public void start() {
		try {
			connectToControlGroup();
			receiveMessage();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void connectToControlGroup() throws IOException,
			UnknownHostException {
		s = new MulticastSocket(port);
		s.joinGroup(InetAddress.getByName(group));
	}

	private void receiveMessage() {
		rec = new ControlStationReceiver(s, this);
	}

	public int setOperatorInfo(String operatorName, int operatorID) {
		if (this.operatorName == "" && this.operatorID == -1) {
			this.operatorName = operatorName;
			this.operatorID = operatorID;

			// operator info was set
			return 1;
		}
		return -1;
	}

	public void setController(ControlStationController stationController) {
		this.stationController = stationController;
	}

	public void processMessage(String msg) {
		System.out.println("Control Station: Message received");
		if (msg.contains("Station locked")) {
			String[] msgParts = msg.split("\\s+");
			try {
				int lockedID = Integer.parseInt(msgParts[msgParts.length - 1]);
				//subtracting one for display purposes (display starts at 0, stations start at 1)
				lockedID = lockedID - stationID; 
				lockedID = lockedID - 1; 
				stationController.updateGUI(lockedID);
			} catch (NumberFormatException nfe) {
				System.out
						.println("The message sent by the station was malformed. \nPlease inform a superior of this problem.");
				nfe.printStackTrace();
			}
		}
	}

	public boolean unlockStation(int lockedID) {
		//transfor from GUI value to actual station ID
		int lockedStation = stationID+lockedID;
		//Value comes from GUI, which starts at 0. 
		lockedStation = lockedStation+1;
		String completeMsg = unlockMsg+lockedStation;
		
		//send packet telling station to unlock
		return sendMessage(completeMsg);
	}

	private boolean sendMessage(String completeMsg) {
		byte buf[] = completeMsg.getBytes(); 
		// Create and send the DatagramPacket
		DatagramPacket pack;
		try {
			pack = new DatagramPacket(buf, buf.length,
					InetAddress.getByName(group), port);
			int ttl = s.getTimeToLive();
			s.setTimeToLive(ttl);
			s.send(pack);
			System.out.println("Control Station: message sent");
			return true;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void initiateVotingStations() {
		sendMessage("Initiate");
		
	}

	public int getQttVotingStations() {
		return qttVotingStations;
	}

	public int getStationID() {
		return stationID;
	}

	public void incrementVotingStations() {
		qttVotingStations++;
	}
}
