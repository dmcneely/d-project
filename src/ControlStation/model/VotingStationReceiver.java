package ControlStation.model;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

public class VotingStationReceiver implements Runnable{
	private VotingStation votingStation;
	private MulticastSocket s;
	private Thread listener = new Thread(this);
	
	public VotingStationReceiver(MulticastSocket s, VotingStation votingStation){
		this.s = s;
		this.votingStation = votingStation;
		
		listener.start();
	}
	
	@Override
	public void run() {
		byte[] buf;
		while(true) 
			try { 
				buf = new byte[1024]; 
				DatagramPacket pack = new DatagramPacket(buf, buf.length);
				s.receive(pack);
				String msg = "";
				msg = new String(pack.getData());
				msg = msg.trim();

				System.out.println(msg);
				System.out.println("Voting Station: message received");
				
				votingStation.processMessage(msg);
			
			} catch(IOException e) { 
					break; 
			} 
	}

}
