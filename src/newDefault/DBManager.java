package newDefault;
// Class 	: DBManager 
// Author 	: Darren Mc Neely 
// 
//

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DBManager 
{

	String host = "jdbc:mysql://localhost:3306/";
	String dbName = "e-voting";
	String username = "root";
	String password = "";
	String url = host + dbName;

	java.sql.Statement stat;
	Connection conn;

	/* Database class handles all the queries */
	public DBManager()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url,username,password);
			stat = conn.createStatement();
		}
		catch(ClassNotFoundException e){
			System.out.printf("Class not found!");
		}

		catch(SQLException e){
			System.out.println("SQL exception!" + e.getMessage());
		}	
	}

	void close()
	{
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Integer[]> getAllVotes() throws SQLException
	{
		ResultSet result = stat.executeQuery("SELECT * FROM vote");
		List<Integer[]> votes = new ArrayList<Integer[]>(); 
		while (result.next()){
		votes.add(new Integer[]{result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4),result.getInt(5),result.getInt(6) ,result.getInt(7), result.getInt(8)});
		}
		return votes;
	}
	public ResultSet getVote(int id) throws SQLException
	{
		ResultSet vote = null;
		ResultSet result = stat.executeQuery("SELECT * FROM vote WHERE id =" + id);
		result.last();
		
		vote = result;
		return vote;
	}
	
	public int getcolumn(int id, int value) throws SQLException
	{
		int column = 0;
		ResultSet result = stat.executeQuery("SELECT * FROM vote");
		List<Integer[]> votes = new ArrayList<Integer[]>(); 
		while (result.next()){
		votes.add(new Integer[]{result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4),result.getInt(5),result.getInt(6) ,result.getInt(7), result.getInt(8)});
		}
		for (int i =0; i < 8; i++)
		{
			if (votes.get(id)[i] == value)
			{
				column = value;
			}
		}
		
		return column;
	}

	public void addVote(int p1, int p2, int p3, int p4, int p5, int p6, int p7) throws SQLException
	{
		stat.execute("INSERT INTO vote VALUES("+ NoOfVotes() + "," +  p1 + "," + p2 + "," + p3+ "," + p4+ "," + p5+ "," + p6+ "," + p7 + ")");
	}

	public void addRandomVotes(int numberofvotes) throws SQLException
	{

		int size = 7;
		int j = 0;
		ArrayList<Integer> list = new ArrayList<Integer>(size);
		for(int i = 1; i <= size; i++) 
		{
			list.add(i);
		}

		while(j <numberofvotes)
		{
			Collections.shuffle(list);
			addVote(list.get(0),list.get(1),list.get(2),list.get(3),list.get(4),list.get(5),list.get(6));
			j++;
		}
	}

	public int NoOfVotes() throws SQLException
	{
		ResultSet counting = stat.executeQuery("SELECT COUNT(*) FROM vote");
		int count = 0;
		while(counting.next()){
			count = counting.getInt(1)+1;
		}
		return count;
	}
	
	public void deleteAllVotes() throws SQLException
	{
		stat.execute("DELETE FROM vote;");
		System.out.println("Removed All Votes!");
	}
	
}