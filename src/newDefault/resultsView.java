package newDefault;
// Class 	: resultsView
// Author 	: Darren Mc Neely 
// 
//

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class resultsView extends JFrame{

	private JPanel contentPane;
	public static JTextArea textArea;
	public static String[][] CandidateEntry;

	public static void main(String args[]) throws SQLException {

		try {
		    new Console();
		} catch (IOException e) {
		}
		new Voting_Algorithm();
		CandidateEntry = new String[Voting_Algorithm.elected.size()][3];
		for(int i = 0; i < Voting_Algorithm.elected.size(); i++)
		{
			CandidateEntry[i] = new String[] {Voting_Algorithm.canNames[Voting_Algorithm.elected.get(i)-1], "../Party" + Voting_Algorithm.elected.get(i) +".png", "../Candidate"+ Voting_Algorithm.elected.get(i) +".png"};
		}

		resultsView frame = new resultsView();
		frame.setDefaultCloseOperation( EXIT_ON_CLOSE );
		frame.setVisible(true);	
		textArea.append(Console.storage);
	}


	@SuppressWarnings("unchecked")
	public resultsView() {

		setTitle("E-voting Application");
		setBounds(100, 100, 731, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblElected = new JLabel("Elected");
		lblElected.setFont(new Font("Times New Roman", Font.ITALIC, 67));
		lblElected.setBackground(Color.WHITE);
		lblElected.setBounds(233, 12, 258, 50);
		contentPane.add(lblElected);
		 

		String[] columnNames = {"Photo", "Name", "Party"};
		Object[][] data = new Object[Voting_Algorithm.elected.size()][3];
		
		for (int i = 0; i < Voting_Algorithm.elected.size(); i++)
		{
		data[i] = new Object[]{new ImageIcon(getClass().getResource(CandidateEntry[i][2])), CandidateEntry[i][0], new ImageIcon(getClass().getResource(CandidateEntry[i][1]))};
		}
		
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(109, 74, 506, 387);
		contentPane.add(tabbedPane);
		JTable table = new JTable( model )
		{
			//  Returning the Class of each column will allow different
			//  renderers to be used based on Class
			@SuppressWarnings("rawtypes")
			public Class getColumnClass(int column)
			{
				return getValueAt(0, column).getClass();
			}
		};
		table.setPreferredScrollableViewportSize(table.getPreferredSize());
		table.setRowHeight(100);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setTableHeader(null);
		table.getColumnModel().getColumn(0).setPreferredWidth(100);
		table.getColumnModel().getColumn(1).setPreferredWidth(280);
		table.getColumnModel().getColumn(2).setPreferredWidth(100);
		table.setFont(new Font("Serif", Font.BOLD, 20));
		JScrollPane scrollPane = new JScrollPane( table );
		tabbedPane.addTab("Elected", null, scrollPane, null);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setRows(20);
		textArea.setColumns(50);
		tabbedPane.addTab("Results", null, new JScrollPane(textArea), null);
	}
}

@SuppressWarnings("serial")
class ImageRenderer extends DefaultTableCellRenderer {
	JLabel lbl = new JLabel();
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		lbl.setIcon((ImageIcon)value);
		return lbl;
	}
}