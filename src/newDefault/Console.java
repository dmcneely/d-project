package newDefault;
// Class 	: Console
// Author 	: Darren Mc Neely 
// 
//

import java.io.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class Console extends JFrame {

	public static String storage = ""; 
	
	public Console() throws IOException {

		redirectSystemStreams();

	}

	//The following codes set where the text get redirected. In this case, jTextArea1    
	private void updateTextArea(final String text) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				storage += text;
			}
		});
	}

	//Followings are The Methods that do the Redirect, you can simply Ignore them. 
	private void redirectSystemStreams() {
		OutputStream out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				updateTextArea(String.valueOf((char) b));
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				updateTextArea(new String(b, off, len));
			}

			@Override
			public void write(byte[] b) throws IOException {
				write(b, 0, b.length);
			}
		};

		System.setOut(new PrintStream(out, true));
		System.setErr(new PrintStream(out, true));
	}
}