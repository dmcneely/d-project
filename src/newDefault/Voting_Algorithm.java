package newDefault;
// Class 	: Voting_Algorithm 
// Author 	: Darren Mc Neely 
// 
//

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Voting_Algorithm {

	public static boolean show_debug = false;		// Debug Mode, prints out each step the algorithm takes 
	public static int quota;						// The Number of votes required to be elected  
	public static int numofseats = 4;				// Number of people to be elected 
	public static int numofCandidates = 7;			// The Number of candidates you can vote for

	public static List<Integer> elected = new ArrayList<Integer>();			// The list of candidates that have met the quota
	public static List<Integer> removedCan = new ArrayList<Integer>();		// The list of candidates that have been removed because they had the lowest number of votes

	public static List<Integer[]> carryvotes = new ArrayList<Integer[]>();	// A list to carry excess votes of candidates that have reached the quota

	@SuppressWarnings("unchecked")
	public static List<Integer>[] CandidateNum = (List<Integer>[])new ArrayList[numofCandidates + 1];	// Holds the id of votes for each candidate 

	public static LocalVotingStation Voting = new LocalVotingStation(show_debug); 						// Class to submit votes  


	public static String[] canNames = new String[] {"Robert Kerns", 			// The names of all the candidates 
		"Charlie Keddy", 
		"Nicky Kelly", 
		"Catherine Kennedy", 
		"Liz McManus", 
		"Raymond John O'Rourke", 
	"Brian Kenny"}; 

	public static String[] canParty = new String[] {"Non-Party", 				// The parties of all the candidates 
		"Non-Party", 
		"The Labour Party", 
		"Socialist Workers Party", 
		"The Labour Party", 
		"Fine Gael", 
	"Non-Party"}; 

	public static DBManager db = new DBManager();		
	public static List<Integer[]> votes;
	public static Integer NoofVotes;

	public Voting_Algorithm() throws SQLException{

		
		votes = db.getAllVotes();
		NoofVotes = db.NoOfVotes()-1;
		quota = (NoofVotes / (numofseats+1)) +1;		// The quota is the number of votes divided by the number of available positions + 1, then add another 1 to the result
		System.out.println("\nNumber of Seats : " + numofseats + "\nQuota : " + quota + "\nNumber of Votes : " + NoofVotes);

		// Initialise array of lists of each candidates votes 
		for (int j = 0; j <8; j++)
		{
			CandidateNum[j] = new ArrayList<Integer>();
		}

		for( int i = 0; i <NoofVotes; i++)
		{
			for (int j = 1; j <8; j++)
			{
				if (votes.get(i)[1] == j)
				{
					CandidateNum[j].add(votes.get(i)[0]);
				}
			}
		}

		int round = 0; 
		System.out.println("\nRound " + (++round));
		for (int j = 1; j <8; j++)
		{
			System.out.println(j + " : " + CandidateNum[j].size());
		}

		while(elected.size() < numofseats)	// Keeps going until all the seats have been filled
		{
			if (add_elect())
			{
				if (numofCandidates - removedCan.size() == numofseats)			//Checks if the number of remaining candidates equals to the number of seats available
				{																//So even if the candidate doesn't meet the quota, they are elected for having the highest remaining number of votes
					for (int j = 1; j <8; j++)
					{
						if (!((removedCan.contains(j)) || (elected.contains(j))))
						{
							elected.add(j);
						}
					}
					break;
				}
				lowest_candidate();
			}
			recalculate_votes();

			System.out.println("\nRound " + (++round));
			for (int j = 1; j <8; j++)
			{
				System.out.println(j + " : " + CandidateNum[j].size());
			}
		}

		db.close();
		System.out.println("\n.............................");
		System.out.println("***********Elected***********");
		for (int k = 0; k < elected.size(); k++)
		{
			System.out.println(elected.get(k) + " : " + canNames[k]);
		}
		System.out.println(".............................");
	}

	public static boolean add_elect() throws SQLException
	{
		boolean nochange = true;
		for (int j = 1; j <8; j++)
		{
			if (elected.size() == numofseats){
				if (show_debug)
					System.out.printf("\nReached number of candidates elected");
				break;
			}

			if (elected.size() == 0)
			{
				if ((CandidateNum[j].size() >= quota))
				{
					elected.add(j);
					if (show_debug)
						System.out.printf("\nCandidate %d has been ELECTED!", j);

					while (CandidateNum[j].size() > quota)
					{
						carryvotes.add(new Integer[] {CandidateNum[j].get(CandidateNum[j].size()-1), j});
						if (show_debug)
							System.out.printf("\nCandidate %d minused a vote", j);

						CandidateNum[j].remove(CandidateNum[j].get(CandidateNum[j].size()-1));
						nochange = false;

					}
				}
			}
			else
				for (int k = 0; k < elected.size(); k++)
				{
					if ((CandidateNum[j].size() >= quota))
					{
						if (!(elected.contains(j)))
						{	
							elected.add(j);
							if (show_debug)
								System.out.printf("\nCandidate %d has been ELECTED!", j);
						}

						while (CandidateNum[j].size() > quota)
						{
							carryvotes.add(new Integer[] {CandidateNum[j].get(CandidateNum[j].size()-1), j});
							if (show_debug)
								System.out.printf("\nCandidate %d minused a vote", j);

							CandidateNum[j].remove(CandidateNum[j].get(CandidateNum[j].size()-1));
							nochange = false;
						}
					}
				}
		}
		return nochange;
	}

	public static void recalculate_votes() throws SQLException
	{
		for(Integer[] list : carryvotes)	// For each vote that needs to be 
		{
			for( int i = 0; i <NoofVotes ; i++)
			{
				if (votes.get(i)[0] == list[0])
				{
					int h = list[1]; 		// Gets the last candidate id number
					int j = getpref(i, h);	// Gets the next voting preference

					if ((CandidateNum[j].size() < quota) && (j != 0))
					{
						if (show_debug)
							System.out.printf("\nCandidate %d given a vote", j);
						CandidateNum[j].add(list[0]);
					}
					else if ((elected.size() < numofseats) && (j != 0))
					{
						elected.add(j);
						j = getpref(i, h);	// Gets the next voting preference

						if (j != 0)
						{
							CandidateNum[j].add(list[0]);
							if (show_debug)
								System.out.printf("\nCandidate %d given a vote", j);
						}
					}	
				}
			}
		}
		carryvotes.clear();	
	}

	public static int getpref(int i ,int h){

		int j = votes.get(i)[h];

		while ((removedCan.contains(j) || elected.contains(j)) && (j != 0))
		{
			if (h == 7)
			{
				if (show_debug)
					System.out.printf("\nVote id : " + votes.get(i)[0]+ " No Preferences Remaining");	
				j = 0;
			}
			else
			{
				j = votes.get(i)[++h];
			}
		}
		return j;
	}

	public static void lowest_candidate() throws SQLException
	{
		int number = quota;
		int can = 0;
		int newcantest = 0, oldcantest = 0;
		for (int j = 1; j <8; j++)
		{
			if ((CandidateNum[j].size() == number) && (CandidateNum[j].size() > 0))
			{
				for( int i = 0; i <NoofVotes; i++)
				{
					if (votes.get(i)[1] == j)
					{
						newcantest++;
					}
					if (votes.get(i)[1] == can)
					{
						oldcantest++;
					}
				}
				
				if (newcantest < oldcantest)
				{
					number = CandidateNum[j].size();
					can = j;
				}

			}

			else if ((CandidateNum[j].size() < number) && (CandidateNum[j].size() > 0))
			{
				number = CandidateNum[j].size();
				can = j;
			}	
		}

		removedCan.add(can);
		if (show_debug)
			System.out.println("\nCandidate "+ can +" with the least number of votes, "+ number);

		while (CandidateNum[can].size() > 0)
		{
			int i = CandidateNum[can].size()-1;
			carryvotes.add(new Integer[] {CandidateNum[can].get(i), can});
			if (show_debug)
				System.out.printf("\nCandidate %d minused a vote", can);
			CandidateNum[can].remove(CandidateNum[can].get(i));
		}
		if (show_debug)
			System.out.printf("\n");
	}

}