package newDefault;
// Class 	: LocalVotingStation
// Author 	: Darren Mc Neely 
// 
//

import java.sql.SQLException;

public class LocalVotingStation {

	public LocalVotingStation(Boolean show_debug) {

		int numberofvotes = 400;			//A set Number of votes		
		DBManager db = new DBManager();		//Connecting to the database

		
		// Delete all votes from the database if a connection is made
		// This is only necessary for testing purposes to keep the number 
		// of votes from becoming to large each time the appication is run
		try {						
			db.deleteAllVotes();			
		} catch (SQLException e) {
			e.printStackTrace();
		}


		// Adds random votes to the database, the number of votes is set by the 'numberofvotes' variable 
		try {
			db.addRandomVotes(numberofvotes );
			System.out.print("Added " + numberofvotes + " random votes\n");
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}	


		// Adds a vote with the preference (1,2,3,4,5,6,7)
		// This is an example on how the voting interface should submit votes 
		// substitute in the candidate id (eg candidate 1 to 7) in order of preference 
		// The candidate 1 is the first preference in this example
		// The candidate 2 is the second .. so on 
		
		/*try {
			db.addVote(1, 2, 3, 4, 5, 6, 7);
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}*/
	}


}
