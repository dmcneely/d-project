package newDefault;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;

import java.awt.GridLayout;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

public class main extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main frame = new main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 550);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(7, 4, 0, 0));
		
		
		JLabel lblParty = new JLabel("PARTY");
		lblParty.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(lblParty);
		
		JLabel lblCandidates = new JLabel("CANDIDATES");
		lblCandidates.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(lblCandidates);
		
		JLabel label = new JLabel("");
		label.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(label);
		
		JLabel lblVotes = new JLabel("VOTES");
		lblVotes.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(lblVotes);
		
		JLabel plb1 = new JLabel("");
		plb1.setIcon(new ImageIcon("Pics/Party2.png"));
		plb1.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(plb1);
		
		JLabel clb1 = new JLabel("<html> KEARNS <br>(ROBERT KEARNS of 4 Castle Park, Wicklow, Co. Wicklow; carpenter </html>");
		clb1.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(clb1);
		
		JLabel piclb1 = new JLabel("");
		piclb1.setIcon(new ImageIcon("Pics/Candidate1.png"));
		piclb1.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(piclb1);
		
		final JSpinner spinner = new JSpinner();
		spinner.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		contentPane.add(spinner);
		
		JLabel plb2 = new JLabel("");
		plb2.setIcon(new ImageIcon("Pics/Party2.png"));
		plb2.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(plb2);
		
		JLabel clb2 = new JLabel("<HTML> KEDDY <br> CHARLIE KEDDY of Sea Road, Kilcoole, Co. Wicklow; Plumber </HTML>");
		clb2.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(clb2);
		
		JLabel piclb2 = new JLabel("");
		piclb2.setIcon(new ImageIcon("Pics/Candidate2.png"));
		piclb2.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(piclb2);
		
		final JSpinner spinner_1 = new JSpinner();
		spinner_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner_1.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		contentPane.add(spinner_1);
		
		JLabel plb3 = new JLabel("");
		plb3.setIcon(new ImageIcon("Pics/Party5.png"));
		plb3.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(plb3);
		
		JLabel clb3 = new JLabel("<HTML> KELLY - THE LABOUR PARTY <br> NICKY KELLY of Aille Highfield Avenue, Arklow, Co Wicklow; Full Time Public Representative </HTML>");
		clb3.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(clb3);
		
		JLabel piclb3 = new JLabel("");
		piclb3.setIcon(new ImageIcon("Pics/Candidate3.png"));
		piclb3.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(piclb3);
		
		final JSpinner spinner_2 = new JSpinner();
		spinner_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner_2.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		contentPane.add(spinner_2);
		
		JLabel plb4 = new JLabel("");
		plb4.setIcon(new ImageIcon("Pics/party1.png"));
		plb4.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(plb4);
		
		JLabel clb4 = new JLabel("<HTML>KENNEDY - SOCIALIST WORKERS PARTY (S.W.P.)<br> CATHERINE KENNEDY of 31 Old Court Drive, Bray, Co. Wicklow </HTML>");
		clb4.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(clb4);
		
		JLabel piclb4 = new JLabel("");
		piclb4.setIcon(new ImageIcon("Pics/Candidate4.png"));
		piclb4.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(piclb4);
		
		final JSpinner spinner_3 = new JSpinner();
		spinner_3.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner_3.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		contentPane.add(spinner_3);
		
		JLabel plb5 = new JLabel("");
		plb5.setIcon(new ImageIcon("Pics/Party5.png"));
		plb5.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(plb5);
		
		JLabel clb5 = new JLabel("<HTML>McManus - the labour party <br> LIZ McMANUS of 1 Martello Terrace, bray, Co. Wicklow; Full Time Public Representative  </HTML>");
		clb5.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(clb5);
		
		JLabel piclb5 = new JLabel("");
		piclb5.setIcon(new ImageIcon("Pics/Candidate5.png"));
		piclb5.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(piclb5);
		
		final JSpinner spinner_4 = new JSpinner();
		spinner_4.setBorder(new LineBorder(new Color(0, 0, 0)));
		spinner_4.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		contentPane.add(spinner_4);
		
		JButton btnvote = new JButton("Vote");
		btnvote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				Integer currentValue =(Integer) spinner_1.getValue();
				if(!(currentValue==0)){
					if(spinner_1.getValue()==spinner.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  2 and 1 are same");
					}
				}
				
				currentValue =(Integer) spinner_2.getValue();
				if(!(currentValue==0)){
					if(spinner_2.getValue()==spinner.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  3 and 1 are same");
					}
					else if(spinner_2.getValue()==spinner_1.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  3 and 2 are same");
					}
				}
				
				currentValue =(Integer) spinner_3.getValue();
				if(!(currentValue==0)){
					if(spinner_3.getValue()==spinner.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  4 and 1 are same");
					}
					else if(spinner_3.getValue()==spinner_1.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  4 and 2 are same");
					}
					else if(spinner_3.getValue()==spinner_2.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  4 and 3 are same");
					}
				}
				
				currentValue =(Integer) spinner_4.getValue();
				if(!(currentValue==0)){
					if(spinner_4.getValue()==spinner.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  5 and 1 are same");
					}
					else if(spinner_4.getValue()==spinner_1.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  5 and 2 are same");
					}
					else if(spinner_4.getValue()==spinner_2.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  5 and 3 are same");
					}
					else if(spinner_4.getValue()==spinner_3.getValue()){
						JOptionPane.showMessageDialog(null,"Error vote number  5 and 4 are same");
					}
				}
				
				
				
				
				
			}
		});
		contentPane.add(btnvote);
	}
}
